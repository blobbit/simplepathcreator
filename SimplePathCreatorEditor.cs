﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Simple Path Creator | Editor Script
/// This makes the custom inspector / editor stuff so that you can create paths for your
/// object with SimplePathCreator script easily in the editor.
/// V1.0 - Nov-2016
/// Author: Antti Järvinen, blob64bit@gmail.com
/// 2016
/// </summary>
[CustomEditor(typeof(SimplePathCreator))]
public class SimplePathCreatorEditor : Editor {
    // static variables for some measurements. You can edit these.
    static float heightOfWpInfo = 100f,
                 maxScrollViewHeight = 400f,
                 ballSize = .2f;

    Vector2 scrollPos;
    bool useballs = true;

    public override void OnInspectorGUI()
    {
        SimplePathCreator spc = (SimplePathCreator)target;

        if (spc.waypoints.Count == 0)
        {
            if (GUILayout.Button("Create path"))
            {
                NewWayPoint(spc, -1);
            }
        } else
        {
            if (GUILayout.Button("Toggle arrows / spheres"))
            {
                useballs = !useballs;
                SceneView.RepaintAll();
            }
            GUILayout.Label("Duration of path: " + spc.TotalDuration.ToString() + " seconds");
            spc.delay = EditorGUILayout.FloatField("Delay", spc.delay);
        }

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(Mathf.Min((spc.waypoints.Count) * heightOfWpInfo, maxScrollViewHeight)));

        for (int i = spc.waypoints.Count - 1; i >= 0; i--)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Waypoint " + i);

            SPC_WayPoint wp = spc.waypoints[i];

            if (GUILayout.Button("Remove", GUILayout.Width(70)))
            {
                spc.waypoints.Remove(wp);
                SceneView.RepaintAll();
                return;
            }

            if (GUILayout.Button("Add", GUILayout.Width(40)))
            {
                NewWayPoint(spc, i);
                return;
            }

            EditorGUILayout.EndHorizontal();


            wp.position = EditorGUILayout.Vector3Field("    Position", wp.position);

            Vector3 pos = Vector3.zero;
            if (i == 0) pos = spc.transform.position;
            else pos = spc.waypoints[i - 1].position;

            float distance = Vector3.Distance(wp.position, pos);

            wp.time = EditorGUILayout.FloatField("    Time", distance / CheckFloat(wp.speed));
            wp.speed = EditorGUILayout.FloatField("    Speed", distance / CheckFloat(wp.time));
            wp.target = EditorGUILayout.ObjectField("    Target (Optional)", wp.target, typeof(Transform), true) as Transform;
        }
        EditorGUILayout.EndScrollView();
    }

    void OnSceneGUI()
    {
        SimplePathCreator spc = target as SimplePathCreator;
        if (spc.enabled) DrawWaypoints(spc);
    }

    void DrawWaypoints(SimplePathCreator spc)
    {
        if (spc.waypoints == null || spc.waypoints.Count == 0) return;

        for (int i = 0; i < spc.waypoints.Count; i++)
        {
            SPC_WayPoint wp = spc.waypoints[i];

            Handles.color = Color.white;

            if (i == 0) Handles.DrawLine(spc.transform.position, wp.position); // For the first waypoint.
            if (i < spc.waypoints.Count - 1) Handles.DrawLine(spc.waypoints[i + 1].position, wp.position);

            if (!spc.hasStarted)
                if (spc.loop = (i == spc.waypoints.Count - 1 && Vector3.Distance(wp.position, spc.transform.position) < 1)) // The last waypoint snaps to the spc-object to create a loop.
                {
                    Handles.color = Color.blue;
                    wp.position = spc.transform.position;
                }


            if (useballs)
                wp.position = Handles.FreeMoveHandle(wp.position, Quaternion.identity, ballSize, Vector3.zero, Handles.SphereCap);
            else
                wp.position = Handles.PositionHandle(wp.position, Quaternion.identity);


            Handles.Label(wp.position, i.ToString());
        }
    }

    void NewWayPoint(SimplePathCreator spc, int i)
    {
        // In case we are adding to the top of the list
        SPC_WayPoint newWp = new SPC_WayPoint();
        if (i == spc.waypoints.Count - 1)
        {
            if (spc.waypoints.Count == 0) newWp.position = spc.transform.position;
            else newWp.position = spc.waypoints[i].position; 

            newWp.position += new Vector3(spc.transform.lossyScale.x, 0, 0);
            spc.waypoints.Add(newWp);
        } else
            {
                SPC_WayPoint wp = spc.waypoints[i];
                newWp.position = wp.position;
                wp.position = Vector3.MoveTowards(wp.position, spc.waypoints[i + 1].position, Vector3.Distance(wp.position, spc.waypoints[i + 1].position) / 2);
                spc.waypoints = spc.AddWaypointAtIndex(newWp, spc.waypoints, i);
            }
        SceneView.RepaintAll();
    }

    /// <summary>
    /// If the user tries to divide by zero
    /// </summary>
    /// <param name="f">Input</param>
    /// <returns></returns>
    float CheckFloat(float f)
    {
        if (f == 0) return 0.0001f;
        return f;
    }
}