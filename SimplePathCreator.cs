﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Simple Path Creator | Object Script
/// Add this to the gameobject you want to move.
/// V1.0 - Nov-2016
/// Author: Antti Järvinen, blob64bit@gmail.com
/// </summary>
public class SimplePathCreator : MonoBehaviour {
    // List of waypoints the object follows.
    public List<SPC_WayPoint> waypoints = new List<SPC_WayPoint>();
    // If delay is -1, the path won't start unless manually started from a script
    public float delay = 0f;
    public bool loop = false;
    private int currentWayPoint = -1;

    public bool hasStarted = false;

    void Start()
    {
        // Error check if user forgets to add waypoints to the list
        if (waypoints.Count == 0 || waypoints == null)
        {
            Debug.LogError(transform.name + " does not have waypoints! Add waypoints to the list in the inspector menu.");
            delay = -1;
        }
    }


    void Update()
    {
        if (delay == -1) return;
        if (delay <= 0)
            NextWaypoint();
        else MoveTowardsWaypoint();
    }

    /// <summary>
    /// Changes the current waypoint to the next one in line.
    /// </summary>
    void NextWaypoint()
    {
        // Check if the waypoint we are trying to access is off the list.
        if (++currentWayPoint == waypoints.Count)
        {
            // if loop-variable is set true, then new waypoint is set to the first one on the list again.
            // Otherwise stop going through waypoints.
            if (!loop)
            {
                delay = -1;
                return;
            }
            currentWayPoint = 0;
        }

        delay = waypoints[currentWayPoint].time;

        if (!hasStarted) hasStarted = true;
    }

    /// <summary>
    /// Moves object towards it's current waypoint and rotates it toward a target waypoint if there's any.
    /// </summary>
    void MoveTowardsWaypoint()
    {
        delay -= Time.deltaTime;
        if (!hasStarted) return;

        SPC_WayPoint wp = waypoints[currentWayPoint];

        if (wp.target != null) transform.LookAt(wp.target.position);

        Vector3 targetPos = wp.position;

        transform.position = Vector3.MoveTowards(transform.position, targetPos, wp.speed * Time.deltaTime);
    }


    /// <summary>
    /// Returns the total duration of waypoints
    /// </summary>
    public float TotalDuration
    {
        get
        {
            if (waypoints.Count == 0) return 0;
            return DurationOfWPList(waypoints);
        }
    }


    /// <summary>
    /// Adds a waypoint to a specific place in the List
    /// </summary>
    /// <param name="wp">Waypoint to add</param>
    /// <param name="wpList">List to add the waypoint to</param>
    /// <param name="index">Where the wp will be placed at in the list.</param>
    /// <returns>Returns updated list</returns>
    public List<SPC_WayPoint> AddWaypointAtIndex(SPC_WayPoint wp, List<SPC_WayPoint> wpList, int index)
    {
        List<SPC_WayPoint> temp = new List<SPC_WayPoint>();

        for (int i = 0; i < wpList.Count; i++)
        {
            if (i == index) temp.Add(wp);
            temp.Add(wpList[i]);
        }

        return temp;
    }

    /// <summary>
    /// Helper function to get total duration of all of the waypoints in given list.
    /// </summary>
    /// <param name="wps">The list of waypoints that we want the total duration to be extracted from.</param>
    /// <returns></returns>
    static float DurationOfWPList(List<SPC_WayPoint> wps)
    {
        float f = 0;
        foreach (SPC_WayPoint wp in wps)
        {
            f += wp.time;
        }
        return f;
    }
}


/// <summary>
/// Waypoint class
/// </summary>
[ExecuteInEditMode]
[System.Serializable]
public class SPC_WayPoint
{
    public Vector3 position;
    public float time = 1, speed = 1;
    public Transform target;
}